// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "HighlightCubeComponent.h"
#include "GameBoardComponent.h"
#include "DogGamePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class DOGGAME_API ADogGamePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
    ADogGamePlayerController();

    void BeginPlay() override;

    void Tick(float DeltaTime) override;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    class UHighlightCubeComponent* highlightCubeComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    class UGameBoardComponent* gameBoardComponent;

private:
    void leftButtonClicked();
    void rightButtonClicked();
    FVector2D getMouseLocation();

};
