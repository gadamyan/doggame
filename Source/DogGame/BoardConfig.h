// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

namespace BoardConfig
{

const int32 cubeCount = 10;
const FVector2D cubeSize = FVector2D(64.f, 64.f);
const FVector2D boardSize = cubeSize * cubeCount;
const FVector2D halfBoardSize = boardSize * 0.5f;
const FBox2D boardBox(-halfBoardSize, halfBoardSize);

inline FIntPoint locationToIndex(const FVector2D& mouseLocation)
{
    FVector2D mouseVector = mouseLocation - boardBox.Min;
    return FIntPoint(static_cast<int>(mouseVector.X / cubeSize.X),
                     static_cast<int>(mouseVector.Y / cubeSize.Y));
}

inline FVector2D indexToCubeLocation(const FIntPoint& index)
{
    return FVector2D(index.X * cubeSize.X, index.Y * cubeSize.Y) +
                    (cubeSize * 0.5f) + boardBox.Min;
}

};
