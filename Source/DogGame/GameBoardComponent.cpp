// Fill out your copyright notice in the Description page of Project Settings.

#include "GameBoardComponent.h"
#include "BoardConfig.h"

UGameBoardComponent::UGameBoardComponent()
{
    using BlueprintFinder = ConstructorHelpers::FObjectFinderOptional<UBlueprint>;
    static BlueprintFinder bpRedDot(TEXT("/Game/Blueprints/BPRed.BPRed"));
    static BlueprintFinder bpGreenDot(TEXT("/Game/Blueprints/BPGreen.BPGreen"));

    UBlueprint *bpRedDotType = bpRedDot.Get();
    if (bpRedDotType)
    {
        redDotClass = bpRedDotType->GeneratedClass;
    }

    UBlueprint *bpGreenDotType = bpGreenDot.Get();
    if (bpGreenDotType)
    {
        greenDotClass = bpGreenDotType->GeneratedClass;
    }
}

void UGameBoardComponent::BeginPlay()
{
    Super::BeginPlay();
    setupAddTimer();
}

void UGameBoardComponent::leftButtonClicked(const FVector2D& mouseLocation)
{
    buttonClicked(mouseLocation, currentDotType == DotType::GREEN);
}

void UGameBoardComponent::rightButtonClicked(const FVector2D& mouseLocation)
{
    buttonClicked(mouseLocation, currentDotType == DotType::RED);
}

void UGameBoardComponent::performMatch()
{
    const FVector2D location = BoardConfig::indexToCubeLocation(
                                        FIntPoint(matchDots.Num(), BoardConfig::cubeCount));
    currentDot->SetActorLocation(FVector(location.X, 0.f, location.Y));
    matchDots.Add(currentDot);
    if (matchDots.Num() == BoardConfig::cubeCount)
    {
        destroyDots();
    }
    currentDot = nullptr;
    currentIndex = FIntPoint(-1, -1);
    clearTimer();
    setupAddTimer();
}

void UGameBoardComponent::performLose()
{
    matchDots.Add(currentDot);
    destroyDots();
    currentDot = nullptr;
    currentIndex = FIntPoint(-1, -1);
    clearTimer();
    setupAddTimer();
}

void UGameBoardComponent::destroyDots()
{
    for (auto dot : matchDots)
    {
        dot->Destroy();
    }
    matchDots.Empty();
}

void UGameBoardComponent::buttonClicked(const FVector2D& mouseLocation, bool matched)
{
    if (currentDot != nullptr && BoardConfig::boardBox.IsInside(mouseLocation) &&
        BoardConfig::locationToIndex(mouseLocation) == currentIndex)
    {
        if (matched)
        {
            performMatch();
        }
        else
        {
            performLose();
        }
    }
}

void UGameBoardComponent::setupAddTimer()
{
    GetOwner()->GetWorldTimerManager().SetTimer(
            currentTimer, this, &UGameBoardComponent::onAddTimer, dotAppearingDelay, false);
}

void UGameBoardComponent::setupRemoveTimer()
{
    GetOwner()->GetWorldTimerManager().SetTimer(
            currentTimer, this, &UGameBoardComponent::onRemoveTimer, dotShowDelay, false);
}

void UGameBoardComponent::clearTimer()
{
    GetOwner()->GetWorldTimerManager().ClearTimer(currentTimer);
}

void UGameBoardComponent::onAddTimer()
{
    currentIndex = FIntPoint(FMath::RandRange(0, 9), FMath::RandRange(0, 9));
    const FVector2D location = BoardConfig::indexToCubeLocation(currentIndex);
    currentDotType = static_cast<DotType>(FMath::RandRange(0, 1));
    spawnDot(location);
    setupRemoveTimer();
}

void UGameBoardComponent::onRemoveTimer()
{
    if (currentDot != nullptr)
    {
        currentDot->Destroy();
        currentDot = nullptr;
        currentIndex = FIntPoint(-1, -1);
    }
    setupAddTimer();
}

void UGameBoardComponent::spawnDot(const FVector2D& location)
{
    UWorld* world = GetWorld();
    if (greenDotClass != nullptr && redDotClass != nullptr && world != nullptr)
    {
        auto actorClass = currentDotType == DotType::RED ? redDotClass : greenDotClass;

        FActorSpawnParameters SpawnParametrs;
        SpawnParametrs.Owner = GetOwner();
        SpawnParametrs.bNoFail = true;

        const FVector worldLocation(location.X, 0.f, location.Y);
        currentDot = world->SpawnActor<AActor>(actorClass, worldLocation,
                                               FRotator::ZeroRotator, SpawnParametrs);
    }
}
