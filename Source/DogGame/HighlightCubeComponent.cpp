// Fill out your copyright notice in the Description page of Project Settings.

#include "HighlightCubeComponent.h"
#include "BoardConfig.h"

UHighlightCubeComponent::UHighlightCubeComponent()
{
    static ConstructorHelpers::FObjectFinderOptional<UBlueprint> bpHighlightCube(
                            TEXT("/Game/Blueprints/BPHighlightCube.BPHighlightCube"));

    UBlueprint *bpHighlightCubeType = bpHighlightCube.Get();

    if (bpHighlightCubeType)
    {
        highlightCubeClass = bpHighlightCubeType->GeneratedClass;
    }
}

void UHighlightCubeComponent::BeginPlay()
{
	Super::BeginPlay();
    spawnHighlightCube();
}

void UHighlightCubeComponent::mouseMoved(const FVector2D& mouseLocation)
{
    if (BoardConfig::boardBox.IsInside(mouseLocation))
    {
        const FIntPoint index = BoardConfig::locationToIndex(mouseLocation);
        const FVector2D location = BoardConfig::indexToCubeLocation(index);
        highlightCube->SetActorLocation(FVector(location.X, 0.f, location.Y));
        highlightCube->SetActorHiddenInGame(false);
    }
    else
    {
        highlightCube->SetActorHiddenInGame(true);
    }
}

void UHighlightCubeComponent::spawnHighlightCube()
{
    if (highlightCube == nullptr && highlightCubeClass != nullptr)
    {
        FActorSpawnParameters SpawnParametrs;
        SpawnParametrs.Owner = GetOwner();
        SpawnParametrs.bNoFail = true;
        UWorld* world = GetWorld();
        if (world)
        {
            highlightCube = world->SpawnActor<AActor>(highlightCubeClass, FVector::ZeroVector,
                                                      FRotator::ZeroRotator, SpawnParametrs);
            highlightCube->SetActorHiddenInGame(true);
        }
    }
}
