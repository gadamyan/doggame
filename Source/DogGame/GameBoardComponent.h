// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameBoardComponent.generated.h"

enum class DotType
{
    RED,
    GREEN
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOGGAME_API UGameBoardComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UGameBoardComponent();
    virtual void BeginPlay() override;

    void leftButtonClicked(const FVector2D& mouseLocation);
    void rightButtonClicked(const FVector2D& mouseLocation);

public:
    UPROPERTY(EditAnywhere, Category = General)
    TSubclassOf<AActor> redDotClass;

    UPROPERTY(EditAnywhere, Category = General)
    TSubclassOf<AActor> greenDotClass;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Block)
    float dotAppearingDelay = 2.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Block)
    float dotShowDelay = 2.0f;

private:
    void setupAddTimer();
    void setupRemoveTimer();
    void clearTimer();
    void onAddTimer();
    void onRemoveTimer();
    void buttonClicked(const FVector2D& mouseLocation, bool matched);
    void spawnDot(const FVector2D& location);
    void performMatch();
    void performLose();
    void destroyDots();

private:
    AActor* currentDot = nullptr;
    TArray<AActor*> matchDots;
    FTimerHandle currentTimer;
    FIntPoint currentIndex = FIntPoint(-1, -1);
    DotType currentDotType;

};
