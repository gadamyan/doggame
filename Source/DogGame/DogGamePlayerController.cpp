// Fill out your copyright notice in the Description page of Project Settings.

#include "DogGamePlayerController.h"

ADogGamePlayerController::ADogGamePlayerController()
{
    PrimaryActorTick.bCanEverTick = true;
    bShowMouseCursor = true;

    highlightCubeComponent = CreateDefaultSubobject<UHighlightCubeComponent>(TEXT("HighlightCube"));
    AddOwnedComponent(highlightCubeComponent);

    gameBoardComponent = CreateDefaultSubobject<UGameBoardComponent>(TEXT("GameBoard"));
    AddOwnedComponent(gameBoardComponent);
}

void ADogGamePlayerController::BeginPlay()
{
    Super::BeginPlay();
    InputComponent->BindAction("LeftButtonClicked", IE_Pressed, this,
                               &ADogGamePlayerController::leftButtonClicked);

    InputComponent->BindAction("RightButtonClicked", IE_Pressed, this,
                               &ADogGamePlayerController::rightButtonClicked);
}

void ADogGamePlayerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    highlightCubeComponent->mouseMoved(getMouseLocation());
}

void ADogGamePlayerController::leftButtonClicked()
{
    gameBoardComponent->leftButtonClicked(getMouseLocation());
}

void ADogGamePlayerController::rightButtonClicked()
{
    gameBoardComponent->rightButtonClicked(getMouseLocation());
}

FVector2D ADogGamePlayerController::getMouseLocation()
{
    FVector mouseWorldLocation;
    FVector mouseWorldDirection;
    DeprojectMousePositionToWorld(mouseWorldLocation, mouseWorldDirection);
    return FVector2D(mouseWorldLocation.X, mouseWorldLocation.Z);
}
