// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HighlightCubeComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOGGAME_API UHighlightCubeComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHighlightCubeComponent();
    virtual void BeginPlay() override;

    void mouseMoved(const FVector2D& mouseLocation);

public:
    UPROPERTY(EditAnywhere, Category = General)
    TSubclassOf<AActor> highlightCubeClass;

private:
    void spawnHighlightCube();

private:
    AActor* highlightCube;
		
};
